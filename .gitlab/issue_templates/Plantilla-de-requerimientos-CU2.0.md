**Código de requermiento**  
(Ej. REQ-16. Donde REQ=requerimiento, 16=numero correlativo unico )  

**Resumen**  
(Resumen corto del requerimiento)  

**Fecha de creación** (Ej. 13/12/2021)  

**Req. Referencial**  
(Es el código referencial que puede ser desde otro sistema de gestion de proyectos)  

**Pre condición**  
(Son un conjunto de condiciones que deben ser ciertas antes de iniciar el requerimiento)  

**Flujo normal (Básico)**  
(P1, P2, P3.... Son los pasos que tiene que cumplir el flujo normal. Usar las palabras podrá, registrará, tendrá etc. )  
*P1.-* Ingresara con usuario Fulano...  
*P2.-* Llenara formulario de registro...  
*P3.-* Enviara información presionando el botón "enviar"...  

**Flujo alterno**  
(AP2 indica que el paso P2 existe un flujo alternativo y se lo antepone la letra A al paso del flujo normal)  
*AP2.-* No puede llenar todos los datos porque no cuenta con todos  

**Criterio de aceptación**  
(Las condiciones que un requerimiento debe satisfacer para ser aceptado por un usuario, cliente o stakeholder)  

**Importancia** (Puede ser: Alta, Media o Baja)  

**Urgencia** (Puede ser: Alta, Media o Baja)  

**Documentos Adjuntos**  
(Se pueden subir archivos o imágenes relacionadas al requerimiento)  


> Esta es una plantilla basica para el llenado de requerimientos de usuario segun [Casos de uso 2.0](https://whyusecases.ivarjacobson.com/files/field_iji_file/article/use_case_2.0_-_spanish_translation.pdf)
