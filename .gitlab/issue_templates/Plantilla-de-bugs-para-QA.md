**Resumen**

(Resumen del error encontrado de manera concisa)


**Enlace de la página**
(Ej.: https://mi.ejemplo.com/security/view/index.php)


**pasos para reproducir error**

(Cómo se puede reproducir el problema: esto es muy importante)

*Paso 1.-* Ingresar con usuario Julano...

*Paso 2.-* Llenar formulario de vista xxx...

*Paso 3.-* Ingresar valores negativos...



**Descripción del error**

(Describir de forma específica el error reportado)


**Comportamiento esperado**

(Es el comportamiento o acción que se esperaría en caso que no exista el error)


**Evidencia o registros de errores (Logs)**

(Pegue los registros relevantes; utilice bloques de código (````) para formatear la salida de la consola,
registros y código, ya que de lo contrario es muy difícil de leer)


**Posibles correcciones**

(Si puede, enlace a la línea de código que podría ser responsable del problema)


