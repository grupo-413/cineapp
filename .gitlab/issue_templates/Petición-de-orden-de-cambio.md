**Código de Orden de Cambio:** (Ingresar el código)  

**Código de Orden de Cambio:** (Ingresar el código)  

**Datos Generales de la Orden de Cambio**  
(Responsables y Fechas)

**Descripción de la Orden de Cambio**  
(Ingresar la descripcion de la Orden de cambio)  

**Justificación de la orden de cambio**  
(Ingresar la justificacion del cambio)  

**Tipo de Cambio**  
(Ingrese el tipo)  

**Elementos Afectados por el Cambio**  
(Ingrese la lista de los elementos afectados)  

**Impacto del Cambio**  
(Ingrese cual fue el impacto por el cambio)  

**Actuación de la Orden de Cambio**  
(Responsables y Fechas)  

**Estado de la Orden de Cambio**  
(En ejecución o Realizada)  


