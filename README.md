### Documentacion
Toda la documentacion del proyecto se encontraria en el siguiente enlace [Link](https://gitlab.com/grupo-413/cineapp-doc)

### Desarrolladores  
D1: Alanoca Elisa  
D2: Cauna Rudy  
D3: Choque Juan C.  
D4: Cuellar Claudia  
D5: Limachi Marcelo  
D6: Oxa Guiselle  
D7: Perez Henry  
Se podran usar los siguientes codigo ejm. D1 para comentar codigo fuente u otros comentarios en el desarrollo

### Gestion de proyectos  
La gestion de proyecto se usaria el tablero en el siguiente enlace [Link](https://gitlab.com/grupo-413/cineapp/-/boards)

### Manual de instalacion o configuracion para el desarrollo  
Para la instalacion, configuracion del entorno de desarrollo y servidores se tiene documentado en el siguiente enlace [Link](https://gitlab.com/grupo-413/cineapp/-/wikis/home)

### Script de apoyo 
Script de ejemplo de codigo, script de migracion, script que automaticen procesos y otros script se encuentra en el siguiente enlace [Link](https://gitlab.com/dashboard/snippets).

### Estandares de nomenclatura para la el desarrollo
Los estandares y nomenclaturas a utilizar en el desarrollo se encuentran en la siguiente paguina [Page](https://gitlab.com/grupo-413/cineapp-doc/-/tree/main/Est%C3%A1ndares%20y%20procedimientos%20de%20ingenier%C3%ADa%20de%20software).

### Configuracion de las herramientas   
Este apartado contiene toda la configuracion para la instalacion de CINEMA APP [Page](https://gitlab.com/grupo-413/cineapp/-/wikis/Configuraciones-de-las-herramientas)...
